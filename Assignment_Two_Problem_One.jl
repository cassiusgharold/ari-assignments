using Markdown
using InteractiveUtils
using AStarSearch
using PlutoUI
using Compat
using DataStructures
using Test

begin 
    using Pkg
    Pkg.activate("project.toml")
    using PlutoUI
end


begin
    mutable struct office
        Id::String
        itemCount::Int64
        neighbourW::Bool
        neighbourE::Bool
        index::Int64
    end

    struct Action #Agent
        Name::String
        Cost::Int64
    end
    
   mutable struct location
        current_O::office
        prev_O::office
        prev_A::Action
        #next_O::office
        
    end	
    
    mutable struct state 
        I_Count::Int64
        S_Cost::Int64
        office_Lot::Vector{Int64}
        A_position::Int64
        heuristicVal::Int64
    end
    
    C3	= office("West_Office", 1, false,true,1)
    C1 	= office("C1_Office", 3, true,true,2)
    C2 	= office("C2_Office", 2, true,true,3)
    C4 	= office("East_Office", 1, true,false,4)
    
    MU = Action("Move Up", 1)
    ME = Action("Move East", 2)
    MW = Action("Move West", 2)
    MD = Action("Move Down", 1)
    CO = Action("Collect", 5)
    
    global Current_position = location(C1,C2,MU)
    global evaluate = true
    
    ItemCount = 7
    officeList = [C3,C1,C2,C4]
    TransitionModel = Dict()
    moves = []
    
    availableActions = Action[CO,MU,ME,MW,MD]
    currentBalance = 5
    stateList = []
    stuffInOffices = [1,3,2,1]
    
# global c_State = state(ItemCount,currentBalance,stuffInOffices,Current_position.current_O.index, 0)
    global c_State = state(6, 5, [1,2,2,1],2,11) 
    global nodeList = state[]
    global closedList = state[]
    
    
    
end 



 if Current_position.prev_A != MU && current_O.itemCount != 0
        push!(actionQue, MU)
end


function move(Action, Old_Location)
    # currentBalance != 0 #Wont run if No Moves made yet
    
    #Moving West
        if Action.Name == "Move West" && Old_Location.current_O.neighbourW == true
#=
        if Old_Location.prev_A == Me && Old_Location.prev_O.itemCount == 0 
            push!(moves, "Usless Move West")
            return
        end
        
=#
        index = Old_Location.current_O.index #Record Index
        
        Old_Location.prev_O = officeList[index] #Record Office Being Left
        
        Old_Location.prev_A = Action #Record Action taken
        
        Old_Location.current_O = officeList[index - 1] #Record Current 
        
        Current_position = Old_Location # Change Location
        
        currentBalance.value = currentBalance.value + Action.Cost #add Cost
        
        push!(moves, Action.Name) #Track movement
        
        #Build New State
        
        #push!(moves, string("New Cost ", currentBalance.value))
        return
    elseif Action.Name == "Move West" && Old_Location.current_O.neighbourW == false
    push!(moves, string("West Move Not Allowed => ", Old_Location.current_O.index))
        return
end
    #Moving East
        if Action.Name == "Move East" && Old_Location.current_O.neighbourE == true
#=
        if Old_Location.prev_A == Mw && Old_Location.prev_O.itemCount == 0 Old_Location.current_O.neighbourW == false
            push!(moves, "Usless Move East")
            return
        end
=#
        index = Old_Location.current_O.index
        Old_Location.prev_O = officeList[index]
        Old_Location.prev_A = Action
        Old_Location.current_O = officeList[index + 1]
        Current_position = Old_Location
        currentBalance.value = currentBalance.value + Action.Cost
        push!(moves, Action.Name) #Track movement
        #push!(moves, string("New Cost ", currentBalance.value))
        return

    elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
    push!(moves, string("East Move Not Allowed => ", Old_Location.current_O.index))
        return
end

    #Collect Action
        if Action.Name == "Collect"
    
        if  Old_Location.prev_A == CO
            push!(moves, "Cannot Collect Twice")
            return
    end
        if Old_Location.current_O.itemCount == 0
            push!(moves, "Office Is Empty")
            return
        end
        
    #If West Office
        if  C3 == Old_Location.current_O
                C3.itemCount = C3.itemCount - 1;
                return
                end
    #If C1 Office
        if C1 == Old_Location.current_O
                C1.itemCount = C1.itemCount - 1;
                return
                end 
    #If C2 Office
        if C2 == Old_Location.current_O
                    C2.itemCount = C2.itemCount - 1;
                return
                end
    #If East Office
        if C4 == Old_Location.current_O
                    C4.itemCount = C4.itemCount - 1;
                return
                end
            
    #Move-Up Action
        if Action.Name == "Move Up"
            
        if Old_Location.current_O.itemCount == 0
            push!(moves, "Cannot Move Up In Empty Office")
            return
    end
        if  Old_Location.prev_A == MU
            push!(moves, "Cannot Move Up Twice")
            return
    end
            
         #If West Office
        if  C3 == Old_Location.current_O
                C3.itemCount = C3.itemCount - 1;
                return
                end
        #If C1 Office
        if C1 == Old_Location.current_O
                C1.itemCount = C1.itemCount - 1;
                return
                end 
        #If C2 Office
        if C2 == Old_Location.current_O
                    C2.itemCount = C2.itemCount - 1;
                return
                end
         #If East Office
        if C4 == Old_Location.current_O
                    C4.itemCount = C4.itemCount - 1;
                return
                end	
            
#=
        push!(moves, "1 Item Collected")
        
        index = Old_Location.current_O.index
        Old_Location.prev_O = officeList[index]
        Old_Location.prev_A = Action
        Old_Location.current_O = officeList[index + 1]
        Current_position = Old_Location
        currentBalance.value = currentBalance.value + Action.Cost
        push!(moves, Action.Name) #Track movement
        #push!(moves, string("New Cost ", currentBalance.value))
        return
    elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
    push!(moves, string("East Move Not Allowed =>", Old_Location.current_O.index))
        return	
            =#
            
        end
        end
    end



function start(c_State)

    # currentBalance != 0 #Wont run if No Moves made yet
    T_state = state()
    
    #Temporary Values To build new States (nodes)
    T_position =  Current_position
    T_Balance = currentBalance
    T_CT = ItemCount
    location = Current_position.current_O.index
    
    for actionz in availableActions
    #Move West
        if actionz == MW && Current_position.current_O.neighbourW == true
        
        index = Current_position.current_O.index #Record Index
        
        T_position.prev_O = officeList[index] #Record Office Being Left
        
        T_position.prev_A = MW #Record Action taken
        
        T_position.current_O = officeList[index - 1] #Record Current 
        
    # Change Location
        
        T_Balance = currentBalance.value + MW.Cost #add Cost
        
        push!(moves, Action.Name) #Track movement
    
    
    #Build New State
        
        T_state.I_Count = T_CT
            
        T_state.S_Cost = T_Balance
        
        T_state.office_Lot = c_State.office_Lot
        
        T_state.A_position = T_position.current_O.index
            
        T_state.heuristicVal = ItemCount  + T_Balance
        
        # for o in officeLot
        # 	push!(T_state.office_Lot, c_State.office_Lot)
        # end
            
            
        return
end
    
    end #End For Loop
    
    #Move East
        if Action.Name == "Move East" && Old_Location.current_O.neighbourE == true
#=
        if Old_Location.prev_A == Mw && Old_Location.prev_O.itemCount == 0 Old_Location.current_O.neighbourW == false
            push!(moves, "Usless Move East")
            return
        end
=#
        index = Old_Location.current_O.index
        Old_Location.prev_O = officeList[index]
        Old_Location.prev_A = Action
        Old_Location.current_O = officeList[index + 1]
        Current_position = Old_Location
        currentBalance.value = currentBalance.value + Action.Cost
        push!(moves, Action.Name) #Track movement
        push!(actionQue, Action.Name)
        #push!(moves, string("New Cost ", currentBalance.value))
        return

    elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
    push!(moves, string("East Move Not Allowed => ", Old_Location.current_O.index))
        return
end
    
    
    #Collect Action
        if Action.Name == "Collect"
    
        if  Old_Location.prev_A == CO
            push!(moves, "Cannot Collect Twice")
            return
    end
        
        if Old_Location.current_O.itemCount == 0
            push!(moves, "Office Is Empty")
            return
        end
        
    #If West Office
        if  C3 == Old_Location.current_O
             C3.itemCount = C3.itemCount - 1;
                return
                end
        
    #If C1 Office
        if C1 == Old_Location.current_O
                C1.itemCount = C1.itemCount - 1;
                return
                end 
        
    #If C2 Office
        if C2 == Old_Location.current_O
                    C2.itemCount = C2.itemCount - 1;
                return
                end
        
    #If East Office
        if C4 == Old_Location.current_O
                    C4.itemCount = C4.itemCount - 1;
                return
                end
            
    #Moving-Up Action
        if Action.Name == "Move Up"
            
        if Old_Location.current_O.itemCount == 0
            push!(moves, "Cannot Move Up In Empty Office")
            return
    end
            
        if  Old_Location.prev_A == MU
            push!(moves, "Cannot Move Up Twice")
            return
    end
            
        #If West Office
        if  C3 == Old_Location.current_O
                C3.itemCount = C3.itemCount - 1;
                return
                end
        #If C1 Office
        if C1 == Old_Location.current_O
                C1.itemCount = C1.itemCount - 1;
                return
                end 
                    #If C2 Office
        if C2 == Old_Location.current_O
                    C2.itemCount = C2.itemCount - 1;
                return
                end
                    #If East Office
        if C4 == Old_Location.current_O
                    C4.itemCount = C4.itemCount - 1;
                return
                end	
            end
        end
    
    # currentBalance = 
    Current_position = T_position
    end



move()

function StateTracker(H_Val, C_Val, office_Lot, A_position)
    if evaluate != true 
        N_state = state(H_Val,C_Val )
        push!(nodeList, N_state)
    
    end
end



function get_Heuristic(ItemCount, totalCost, action)
   if action == Co 
        x = Item
    end
end

begin
    Cr = state(6, 6, [1,2,2,1],2,12) #
    Cw = state(6, 8, [1,2,2,1],1,14) #
    Ce = state(6, 8, [1,2,2,1],3,14) #
end


begin 
    push!(nodeList, Cr)
    push!(nodeList, Cw)
    push!(nodeList, Ce)
end


with_terminal() do
    println(nodeList)
end



function evaluateOpenList(c_State)
    T_cost = currentBalance
    T_ItemCount = c_State.I_Count
         if evaluate == true
        
        for node in nodeList
            
            if node.I_Count != T_ItemCount
                
                if node.I_Count < T_ItemCount
                    push!(closedList, node)
                        c_State = node
                    push!(moves, node)
                    return
                end
                
            end
            
        end
        
        for node in nodeList
        push!(moves, node.I_Count == T_ItemCount)
            if node.I_Count == T_ItemCount
                        push!(moves,string("h=>",node.heuristicVal, "h2=>",T_cost))
                if node.heuristicVal <= T_val
                    T_Hval = node.heuristicVal
                        c_State = node
                    push!(closedList, node)
                    push!(moves, "low")
                    return
                end
                
            end
            
        end
        
        
            
        
    end
end


function openList(aState)

    if evaluate == false
        push!(nodeList, N_state)
    return true
    elseif evalute == true
        evaluateOpenList()
    end
end


evaluateOpenList(c_State)

with_terminal() do
    println(moves)
end


with_terminal() do
    println(closedList)
    
end


with_terminal() do
    println(c_State)
    
end



begin
	move(MW,Current_position)
	move(MW,Current_position)
	move(ME,Current_position)
	move(ME,Current_position)
	move(ME,Current_position)
	move(MW,Current_position)
end


#=with_terminal() do 
   println(string(
            "Action Taken => ",  moves,"\n",
            "Location => ",Current_position.current_O.Id, "\n",
            "Previous Node => ", Current_position.prev_O.Id,"\n",
            "Previous Action => ", Current_position.prev_A.Name,"\n",
            "Office Arrangement =>", "\n",
            "Cost To Get Here => ", currentBalance))
=#end


#R = state(7, 1, [1,3,2,1],2)








