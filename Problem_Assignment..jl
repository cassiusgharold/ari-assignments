using Pkg


using CSV, DataFrames, PlutoUI, Statistics, StatsBase, Plots, Flux

#In this cell, we are getting the location of our file as well as loading the dataset

cassius_df = CSV.read("C:\\Users\\26481\\Desktop\\NUST Modules\\Fith Semester 5\\ARTIFICIAL INTELLIGENCE\\assignments\\Real estate.csv", DataFrame)

begin
    rename!(cassius_df, Symbol.(replace.(string.(names(cassius_df)), Ref(r"\[m\]"=>"_"))))
    rename!(cassius_df, Symbol.(replace.(string.(names(cassius_df)), Ref(r"\[s\]"=>"_"))))
    rename!(cassius_df, Symbol.(replace.(string.(names(cassius_df)), Ref(r"\s"=>"_"))))
end









X = cassius_df[:,3:7]


cor(Matrix(cassius_df))


Ano_data = Matrix(cassius_df[:,3:8])


function get_scaling_params(init_feature_mat)
    feature_mean = mean(init_feature_mat, dims=1)
    f_dev = std(init_feature_mat, dims=1)
    return (feature_mean, f_dev)
end


function scale_features(feature_mat, sc_params)
    normalised_feature_mat = (feature_mat .- sc_params[1]) ./ sc_params[2]
end


X_mat = Matrix(X)

Y = cassius_df[:,8]

all_data_size = size(X_mat)[1]

training_size = 0.7

training_index = trunc(Int, training_size * all_data_size)

#NB: Anywhere its written 'Ano' its short for Another.
Ano_train = Ano_data[1:training_index, :]


Ano_test = Ano_data[1+training_index:end, :]

sc_params = get_scaling_params(Ano_train[:, 1:end-1])


 sc_mat_1 = scale_features(Ano_train[:, 1:end-1], sc_params)


sc_mat_2 = scale_features(Ano_test[:, 1:end-1], sc_params)

train_feature = transpose(sc_mat_1)

test_feature = transpose(sc_mat_2)


train_label = Ano_train[:, end]

test_label = Ano_test[:, end]


size(test_label)

using Flux.Data

using Flux: @epochs

Pkg.activate("Project.toml")

dl_train = DataLoader((train_feature, train_label), batchsize=50)

 dl_test = DataLoader((test_feature, test_label), batchsize=10)

X_mat_train = X_mat[1:training_index, :]

size(X_mat_train)

X_mat_test = X_mat[1+training_index:end, :]

Y_vec_train = Y[1:training_index]


size(Y_vec_train)

Y_vec_test = Y[1+training_index:end]

# re_m = Dense(5,1)
re_m = Chain(Dense(5,2,σ), Dense(2,1))

loss(x,y) = Flux.Losses.mse(re_m(x), y)

ps = Flux.params(re_m)


 opt = Descent(0.1)

 @epochs 30 Flux.train!(loss, ps, dl_train, opt)

pred2 = re_m(test_feature)

function root_mean_square_error(actual_outcome, predicted_outcome)
    errors = predicted_outcome - actual_outcome
    squared_errors = errors .^ 2
    mean_squared_errors = mean(squared_errors)
end












