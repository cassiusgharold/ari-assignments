using Markdown
using InteractiveUtils

using DataFrames, CSV,Statistics, StatsBase, Plots, Flux , Random, Images

using  LazyStack: stack, rstack

begin
    train_norm = load_image_batch("C:\\Users\\26481\\Desktop\\NUST Modules\\Fith Semester 5\\ARTIFICIAL INTELLIGENCE\\assignments\\Assignment 2\\chest_xray\\train\\NORMAL")
    train_monia = load_image_batch("C:\\Users\\26481\\Desktop\\NUST Modules\\Fith Semester 5\\ARTIFICIAL INTELLIGENCE\\assignments\\Assignment 2\\chest_xray\\train\\PNEUMONIA")
    x_train = prepare_data(vcat(train_norm, train_monia))
    y_train = Flux.onehotbatch(vcat(
    zeros(Int8, size(train_norm)),
    ones(Int8, size(train_monia)),
  ), 0:1)
end

begin
    test_norm = load_image_batch("C:\\Users\\26481\\Desktop\\NUST Modules\\Fith Semester 5\\ARTIFICIAL INTELLIGENCE\\assignments\\Assignment 2\\chest_xray\\test\\NORMAL")
    test_monia = load_image_batch("C:\\Users\\26481\\Desktop\\NUST Modules\\Fith Semester 5\\ARTIFICIAL INTELLIGENCE\\assignments\\Assignment 2\\chest_xray\\test\\PNEUMONIA")
    x_test = prepare_data(vcat(test_norm, test_monia))
    y_test = Flux.onehotbatch(vcat(
    zeros(Int8, size(test_norm)),
    ones(Int8, size(test_monia)),
  ), 0:1)
end

begin
    val_normal = load_image_batch("C:\\Users\\26481\\Desktop\\NUST Modules\\Fith Semester 5\\ARTIFICIAL INTELLIGENCE\\assignments\\Assignment 2\\chest_xray\\val\\NORMAL")
    val_pneumonia = load_image_batch("C:\\Users\\26481\\Desktop\\NUST Modules\\Fith Semester 5\\ARTIFICIAL INTELLIGENCE\\assignments\\Assignment 2\\chest_xray\\val\\PNEUMONIA")
    x_val = prepare_data(vcat(val_normal, val_pneumonia))
    y_val = Flux.onehotbatch(vcat(
    zeros(Int8, size(val_normal)),
    ones(Int8, size(val_pneumonia)),
  ), 0:1)
end

function img_to_array(img)
  return reshape(real.(Gray.(img)), size(img)..., 1)
end

function prepare_data(data)
  converted = img_to_array.(data)
  return rstack(convert.(Array{Float32}, converted))
end

function resize(img)
  return Images.imresize(img, (224, 224)...)
end

function load_image_batch(path::String)
  images = []
  for img_path in readdir(path, join=true)
    push!(images, resize(load(img_path)))
  end
  return images
end

accuracy(x, y, m) = mean(Flux.onecold(m(x), 0:1) .== Flux.onecold(y, 0:1))


samples = rand(1:size(x_train)[4], 10)

begin
  model = Chain(
    Conv((11, 11), 1 => 96, stride=4, relu),
    MaxPool((3, 3), stride=2),
    Conv((5, 5), 96 => 256, pad=2, relu),
    MaxPool((3, 3), stride=2),
    Conv((3, 3), 256 => 384, pad=1, relu),
    Conv((3, 3), 384 => 384, pad=1, relu),
    Conv((3, 3), 384 => 256, pad=1, relu),
    MaxPool((3, 3), stride=2),
    Flux.flatten,
    Dense(6400, 4096, relu),
    Dropout(0.5),
    Dense(4096, 4096, relu),
    Dropout(0.5),
    Dense(4096, 2),
    softmax
  )
  opt = Momentum()
  loss(x, y) = sum(Flux.crossentropy(model(x), y))
  data_loader = Flux.Data.DataLoader((x_train[:, :, :, samples], y_train[:, samples]), batchsize=10, shuffle=true)
  for epoch in 1:20
    for d in data_loader
      gs = gradient(params(model)) do
        l = loss(d...)
      end
      Flux.update!(opt, params(model), gs)
    end
  end
end


begin
    mse = Flux.Losses.mse(y_test, model(x_test))
    rmse = sqrt(mse)
    accuracy(x_test, y_test, model)
end






